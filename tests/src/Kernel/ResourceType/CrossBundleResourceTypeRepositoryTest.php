<?php

namespace Drupal\Tests\jsonapi_cross_bundles\Kernel\ResourceType;

use Drupal\jsonapi\ResourceType\ResourceType;
use Drupal\Tests\jsonapi_cross_bundles\Kernel\JsonapiCrossBundlesTestBase;

/**
 * Tests the cross bundle repository.
 *
 * @group jsonapi_cross_bundles
 */
class CrossBundleResourceTypeRepositoryTest extends JsonapiCrossBundlesTestBase {

  /**
   * Tests the cross bundle resource types available.
   */
  public function testRootEntityTypeResourceType() {
    $resource_type_repository = $this->container->get('jsonapi.resource_type.repository');
    $resource_types = $resource_type_repository->all();
    $filtered_resource_types = array_filter($resource_types, static function (ResourceType $resource_type) {
      return $resource_type->getEntityTypeId() === 'entity_test' && $resource_type->getBundle() === 'entity_test';
    });
    $this->assertCount(1, $filtered_resource_types, 'Entity type with bundle support has a root resource type.');
    $filtered_resource_types = array_filter($resource_types, static function (ResourceType $resource_type) {
      return $resource_type->getEntityTypeId() === 'user' && $resource_type->getBundle() === NULL;
    });
    $this->assertCount(0, $filtered_resource_types, 'Entity type without bundle support does not have root resource type.');
  }

}
