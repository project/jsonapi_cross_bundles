<?php

namespace Drupal\Tests\jsonapi_cross_bundles\Unit;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\jsonapi_cross_bundles\ResourceType\CrossBundlesResourceType;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the CrossBundleResourceType resource type.
 *
 * @group jsonapi_cross_bundles
 *
 * @coversDefaultClass \Drupal\jsonapi_cross_bundles\ResourceType\CrossBundlesResourceType
 */
final class CrossBundleResourceTypeTest extends UnitTestCase {

  /**
   * @covers ::getBundle
   */
  public function testGetBundle() {
    $resource_type = new CrossBundlesResourceType(
      'entity_test',
      'entity_test',
      EntityTest::class
    );
    $this->assertNull($resource_type->getBundle());
  }

  /**
   * @covers ::getPath
   */
  public function testGetPath() {
    $resource_type = new CrossBundlesResourceType(
      'entity_test',
      'entity_test',
      EntityTest::class
    );
    $this->assertEquals('entity_test', $resource_type->getPath());
  }

  /**
   * @covers ::isMutable
   */
  public function testMutability() {
    $resource_type = new CrossBundlesResourceType(
      'entity_test',
      'entity_test',
      EntityTest::class
    );
    $this->assertFalse($resource_type->isMutable());
  }

}
