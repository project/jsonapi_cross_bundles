<?php

namespace Drupal\jsonapi_cross_bundles\ResourceType;

use Drupal\jsonapi\ResourceType\ResourceType;

/**
 * Resource type that allows collections for all bundles in an entity type.
 */
final class CrossBundlesResourceType extends ResourceType {

  /**
   * The resource types that this resource type aggregates.
   *
   * @var \Drupal\jsonapi\ResourceType\ResourceType[]
   */
  protected $bundleResourceTypes;

  /**
   * {@inheritdoc}
   */
  public function getTypeName() {
    // This way the name isn't `{entity_type_id}--`
    return $this->entityTypeId;
  }

  /**
   * Sets the cross bundle resource types.
   *
   * @param array $bundle_resource_types
   *   An array of the bundle-specific resource types that this object
   *   aggregates.
   *
   * @return static
   */
  public function setBundleResourceTypes(array $bundle_resource_types) {
    $this->bundleResourceTypes = $bundle_resource_types;
  }

  /**
   * Gets individual JSON:API resource types that this resource type aggregates.
   *
   * @return \Drupal\jsonapi\ResourceType\ResourceType[]
   *   An array of bundle-specific resource types.
   */
  public function getBundleResourceTypes() {
    if (!isset($this->bundleResourceTypes)) {
      throw new \LogicException("setBundleResourceTypes() must be called before getting the bundle resource types.");
    }
    return $this->bundleResourceTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->entityTypeId;
  }

  /**
   * {@inheritdoc}
   */
  public function getBundle() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function isMutable() {
    return FALSE;
  }

}
