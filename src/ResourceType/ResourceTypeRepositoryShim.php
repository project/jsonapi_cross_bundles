<?php

namespace Drupal\jsonapi_cross_bundles\ResourceType;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\jsonapi\ResourceType\ResourceType;
use Drupal\jsonapi\ResourceType\ResourceTypeRepository;

/**
 * Exposes some protected methods from the core resource type repository.
 *
 * @internal
 */
final class ResourceTypeRepositoryShim extends ResourceTypeRepository {

  /**
   * {@inheritdoc}
   */
  public function getFields(array $field_names, EntityTypeInterface $entity_type, $bundle) {
    return parent::getFields($field_names, $entity_type, $bundle);
  }

  /**
   * {@inheritdoc}
   */
  public function getAllFieldNames(EntityTypeInterface $entity_type, $bundle) {
    return parent::getAllFieldNames($entity_type, $bundle);
  }

  public function calculateRelatableResourceTypes(ResourceType $resource_type, array $resource_types) {
    return parent::calculateRelatableResourceTypes($resource_type, $resource_types);
  }

}
